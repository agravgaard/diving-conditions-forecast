const options = {
    // Required: API key
    key: 'VCiudH8DQCa6TARuqFofxXjgTpbvQsy4', // REPLACE WITH YOUR KEY !!!

    // Put additional console output
    verbose: true,

    // Optional: Initial state of the map
    lat: 56.1,
    lon: 10.2,
    zoom: 5,
};

// Initialize Windy API
windyInit(options, windyAPI => {
    const { map, picker, utils, overlays, broadcast } = windyAPI;

    overlays.wind.setMetric('m/s');

    picker.on('pickerOpened', latLon => {
        // picker has been opened at latLon coords
        console.log(latLon);

        const { lat, lon, values, overlay } = picker.getParams();
        // -> 48.4, 14.3, [ U,V, ], 'wind'
        console.log(lat, lon, values, overlay);

        const windObject = utils.wind2obj(values);
        console.log(windObject);
    });

    broadcast.once('redrawFinished', () => {
        picker.open({ lat: 56.1, lon: 10.2 });
        // Opening of a picker (async)
    });

});
